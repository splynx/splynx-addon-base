#!/bin/bash

DEFAULT_VERSION='3.1'

read -rp "Enter major version to bump [${DEFAULT_VERSION}]: " VERSION
VERSION=${VERSION:-${DEFAULT_VERSION}}
LAST_TAG=$(git tag | grep "$VERSION" | tail -1)
VERSION=${LAST_TAG:-'0.0.0'}
#Get number parts
MAJOR="${VERSION%%.*}"
VERSION="${VERSION#*.}"
MINOR="${VERSION%%.*}"
VERSION="${VERSION#*.}"
PATCH="${VERSION%%.*}"
VERSION="${VERSION#*.}"

#Increase version
PATCH=$((PATCH + 1))
NEW_TAG="$MAJOR.$MINOR.$PATCH"

echo "Bumping from $LAST_TAG to $NEW_TAG"

DELETE_PREVIOUS_DEFAULT='N'
read -rp "Delete previous tag? Answer 'y' or 'n' [${DELETE_PREVIOUS_DEFAULT}]: " DELETE_PREVIOUS
DELETE_PREVIOUS=${DELETE_PREVIOUS:-${DELETE_PREVIOUS_DEFAULT}}

case $DELETE_PREVIOUS in
[Yy]*)
  echo "Deleting previous tag.."
  git push --delete origin "$LAST_TAG"
  git tag --delete "$LAST_TAG"
  ;;
esac

echo "Adding new tag.."
git tag "$NEW_TAG"
git push --tags
